<?php

namespace Drupal\vais_promos;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for vais_promo entity.
 *
 * @ingroup vais_promos
 */
class VaisPromoListBuilder extends EntityListBuilder {

  /**
   * Information about the entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_type, $storage);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   *
   * We override ::render() so that we can add our own content above the table.
   * parent::render() is where EntityListBuilder creates the table using our
   * buildHeader() and buildRow() implementations.
   */
  public function render() {
    $build['description'] = [
      '#markup' => $this->t('Search Promotions can be used on Vertex AI Search Pages.'),
    ];

    $build += parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   *
   * Building the header and content lines for the vais_promo list.
   *
   * Calling the parent::buildHeader() adds a column for the possible actions
   * and inserts the 'edit' and 'delete' links as defined for the entity type.
   */
  public function buildHeader() {
    $header['promo_search_page'] = $this->t('Promotion Search Page');
    $header['promo_trigger'] = $this->t('Promotion Trigger Query');
    $header['promo_reference_type'] = $this->t('Promotion Reference Type');
    $header['promo_reference_link'] = $this->t('Promotion Reference Link');
    $header['promo_title_override'] = $this->t('Promotion Title');
    $header['promo_description'] = $this->t('Promotion Description');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {

    $promoLink = Link::createFromRoute($entity->promo_trigger->value, 'entity.vais_promo.canonical', ['vais_promo' => $entity->id()]);

    $promoType = $entity->promo_type->value;
    $promoReferenceType = $this->t('Internal');
    $promoReferenceLink = NULL;

    // Get link to referenced content; either internal node or external link.
    if ($promoType == 'external') {
      $promoReferenceUrl = Url::fromUri($entity->promo_link->value);
      $promoReferenceType = $this->t('External');
      $promoReferenceLink = Link::fromTextAndUrl($entity->promo_link->value, $promoReferenceUrl);
    }
    else {
      $promoContent = $this->entityTypeManager->getStorage('node')->load($entity->promo_content->target_id);
      $promoReferenceLink = Link::createFromRoute($promoContent->label(), 'entity.node.canonical', ['node' => $entity->promo_content->target_id]);
    }

    $searchPage = $this->entityTypeManager->getStorage('search_page')->load($entity->promo_search_page->value);
    $searchPageLink = Link::createFromRoute($searchPage->label(), 'entity.search_page.edit_form', ['search_page' => $entity->promo_search_page->value]);

    $row['promo_search_page'] = $searchPageLink;
    $row['promo_trigger'] = $promoLink;
    $row['promo_reference_type'] = $promoReferenceType;
    $row['promo_reference_link'] = $promoReferenceLink;
    $row['promo_title_override'] = $entity->promo_title_override->value;
    $row['promo_description'] = $entity->promo_description->value;

    return $row + parent::buildRow($entity);
  }

}
