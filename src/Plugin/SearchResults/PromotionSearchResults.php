<?php

namespace Drupal\vais_promos\Plugin\SearchResults;

use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\vertex_ai_search\Plugin\VertexSearchResultsPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides simple autocomplete based on node title and/or description.
 *
 * @VertexSearchResultsPlugin(
 *   id = "vertex_search_results_promotions",
 *   title = @Translation("Adding Promotions to Search Results"),
 *   weight = 0
 * )
 */
class PromotionSearchResults extends VertexSearchResultsPluginBase {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $eTypeManager;

  /**
   * Database Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * Path Alias Manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * Constructor.
   *
   * @param array $configuration
   *   Configuration array containing information about search page.
   * @param string $plugin_id
   *   Identifier of custom plugin.
   * @param array $plugin_definition
   *   Provides definition of search plugin.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity Type Manager service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection service.
   * @param \Drupal\path_alias\AliasManagerInterface $aliasManager
   *   The path alias manager service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    Connection $database,
    AliasManagerInterface $aliasManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->eTypeManager = $entityTypeManager;
    $this->database = $database;
    $this->aliasManager = $aliasManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->get('path_alias.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function modifyPageResults(string $keyword, array $searchResults, string $search_page_id) {

    $query = $this->database->select('vais_promo', 'vp');
    $query->condition('vp.promo_search_page', $search_page_id, '=');
    $query->addField('vp', 'pid', 'pid');
    $query->addField('vp', 'promo_trigger', 'trigger');

    $results = $query->execute()->fetchAll();

    $promos = [];

    foreach ($results as $result) {
      $triggerWords = explode(',', $result->trigger);

      foreach ($triggerWords as $triggerWord) {
        $promos[trim($triggerWord)][] = $result->pid;
      }

    }

    // See if a promotion is available for the search keyword.
    if (empty($promos[$keyword])) {
      return $searchResults;
    }

    // Load the relevant promo.
    $vaisPromos = $this->eTypeManager->getStorage('vais_promo')->loadMultiple($promos[$keyword]);

    foreach ($vaisPromos as $vaisPromo) {
      $promoType = $vaisPromo->get('promo_type')->value;

      // Set the promo reference to the external link.
      $promoRef = $vaisPromo->get('promo_link')->value;

      // If the promotion uses referenced content, get the link.
      if ($promoType == 'internal') {

        // Load the promotion referenced content.
        $promoContentId = $vaisPromo->get('promo_content')->target_id;
        $promoRef = $this->aliasManager->getAliasByPath('/node/' . $promoContentId);

      }

      // Create the promotion result.
      $promotion = [
        '#theme' => 'vais_promos_promotion',
        '#title' => $vaisPromo->get('promo_title_override')->value,
        '#description' => $vaisPromo->get('promo_description')->value,
        '#promo_type' => $promoType,
        '#href' => $promoRef,
        '#term' => $keyword,
        '#plugin_id' => $this->getPluginId(),
        '#attached' => [
          'library' => [
            'vais_promos/vaisPromos',
          ],
        ],
      ];

      // Inserts promotion after any messages that may exist.
      foreach ($searchResults as $key => $searchResult) {
        if (!empty($searchResult['#result'])) {
          array_splice($searchResults, $key, 0, [$promotion]);
          break;
        }
      }

    }

    return $searchResults;

  }

}
