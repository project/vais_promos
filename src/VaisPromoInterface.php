<?php

namespace Drupal\vais_promos;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a Vertex AI Search Promo entity.
 *
 * @ingroup vais_promos
 */
interface VaisPromoInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
