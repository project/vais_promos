<?php

namespace Drupal\vais_promos\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the vais_promo entity edit forms.
 *
 * @ingroup vais_promos
 */
class VaisPromoForm extends ContentEntityForm {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The constructor for the Form.
   *
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entity_type_bundle_info
   *   The entity type bundle service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time, Connection $database) {
    parent::__construct(
      $entity_repository,
      $entity_type_bundle_info,
      $time
    );
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time'),
      $container->get('database')
    );

  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\vais_promos\Entity\VaisPromo $entity */
    $form = parent::buildForm($form, $form_state);
    $entity = $this->entity;

    $form['promo_content']['#states'] = [
      'visible' => [
        ':input[name="promo_type"]' => ['value' => 'internal'],
      ],
    ];

    $form['promo_link']['#states'] = [
      'visible' => [
        ':input[name="promo_type"]' => ['value' => 'external'],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    $entity = parent::validateForm($form, $form_state);

    // Validate referenced content (internal) or link (external) of promotion.
    if ($form_state->getValue('promo_type')[0]['value'] == 'internal') {

      if (empty($form_state->getValue('promo_content')[0]['target_id'])) {
        $form_state->setErrorByName('promo_content',
          $this->t('Reference a content item to be used by the promo.'));
      }

    }
    elseif ($form_state->getValue('promo_type')[0]['value'] == 'external') {

      if (empty($form_state->getValue('promo_link')[0]['value'])) {
        $form_state->setErrorByName('promo_link',
          $this->t('Specify an external URL to be used by the promo.'));
      }
      elseif (!UrlHelper::isValid($form_state->getValue('promo_link')[0]['value'], TRUE)) {
        $form_state->setErrorByName('promo_link',
          $this->t('Please provide a valid and full URL.'));
      }

    }

    // Ensure numeric values.
    if ($form_state->hasValue('weight') && !is_numeric($form_state->getValue('weight'))) {
      $form_state->setErrorByName('weight', $this->t('Weight value must be numeric.'));
    }

    return $entity;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $promoTrigger = $form_state->getValue('promo_trigger')[0]['value'];

    // Set all trigger words to lowercase.
    $form_state->setValue(
      'promo_trigger',
        [['value' => strtolower($promoTrigger)]]
    );

    // Remove value for unneeded promo reference type.
    if ($form_state->getValue('promo_type')[0]['value'] == 'external') {
      $form_state->setValue('promo_content', [['target_id' => NULL]]);
    }
    elseif ($form_state->getValue('promo_type')[0]['value'] == 'internal') {
      $form_state->setValue('promo_link', [['value' => NULL]]);
    }

    parent::submitForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $status = parent::save($form, $form_state);

    $entity = $this->entity;
    if ($status == SAVED_UPDATED) {
      $this->messenger()
        ->addMessage($this->t('The promo %feed has been updated.', ['%feed' => $entity->toLink()->toString()]));
    }
    else {
      $this->messenger()
        ->addMessage($this->t('The promo %feed has been added.', ['%feed' => $entity->toLink()->toString()]));
    }

    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $status;
  }

}
