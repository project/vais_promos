<?php

namespace Drupal\vais_promos\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VaisPromoSettingsForm manages settings.
 *
 * @package Drupal\vais_promos\Form
 *
 * @ingroup vais_promos
 */
class VaisPromoSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The constructor for the settings form.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager interface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typed_config_manager, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {

    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager')
    );

  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'vais_promo_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['vais_promos.entity_type_settings'];
  }

  /**
   * Define the form used for VaisPromo settings.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    $options = [];
    foreach ($types as $type => $typeObject) {
      $options[$type] = $typeObject->label();
    }

    $form['vais_promotion_types'] = [
      '#title' => $this->t('Promotion Content Types'),
      '#type' => 'details',
      '#open' => TRUE,
    ];

    $form['vais_promotion_types']['vais_promo_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Valid Types'),
      '#description' => $this->t(
        'Select the content types that can serve as Promos.
        All types are valid if none are selected.'
      ),
      '#options' => $options,
      '#multiple' => TRUE,
      '#default_value' => $this->config('vais_promos.entity_type_settings')->get('vais_promo_types'),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit_settings'] = [
      '#type' => 'submit',
      '#name' => 'vais_promo_settings',
      '#button_type' => 'primary',
      '#value' => $this->t('Submit'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('vais_promos.entity_type_settings')->set('vais_promo_types', $form_state->getValue('vais_promo_types'))->save();

    parent::submitForm($form, $form_state);

  }

}
