<?php

declare(strict_types=1);

namespace Drupal\Tests\vais_promos\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests the Import From XML form.
 */
class VaisImportXmlFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'options',
    'search',
    'vertex_ai_search',
    'vais_promos',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'olivero';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->drupalCreateUser([
      'administer search',
      'access administration pages',
      'access content',
      'manage vertex promotions',
    ]));
  }

  /**
   * Test we can go to the main page.
   */
  public function testVaisImportXmlFormMainPage(): void {
    $this->drupalGet('admin/content/vais_promo');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Test we can go to the form and the fields are present.
   */
  public function testVaisImportXmlFormFieldsExist() {
    $this->drupalGet('admin/content/vais_promo/import');
    $this->assertSession()->statusCodeEquals(200);

    $this->assertSession()->fieldExists('edit-promo-search-page');
    $this->assertSession()->fieldExists('edit-import-xml');
    $this->assertSession()->elementExists('css', '#edit-submit');
  }

}
