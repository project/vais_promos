# Vertex AI Search Promoted Results

This module works in conjunction with the [Vertex AI Search](https://www.drupal.org/project/vertex_ai_search) module to provide promoted search result capabilities to
Vertex AI Search custom search pages.  The Site Restricted JSON API used to integrate with the [Google Programmable Search Engine](https://developers.google.com/custom-search/docs/overview) will cease to operate on December 18, 2024; Google recommends Vertex AI Search as a replacement.  As there is no counterpart to the 
promoted search result functionality provided by the Google Programmable Search Engine, this module was developed to fill the gap.

This Drupal module defines a custom entity to represent a Promoted Search Result and provides a custom search results plugin that places a promoted search
result (if one exists for the search keyword) at the top of a Vertex AI Search custom search results page.

For a full description of the Drupal module, visit the
[Vertex AI Search Promoted Results](https://www.drupal.org/project/vais_promos).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/vais_promos).

[Documentation for the Vertex AI Search Promoted Results module](https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/vertex-ai-search-promoted-results) is available on Drupal.org.


## Table of contents

- Requirements
- Installation
- Configuration and Usage
- Maintainers
- Developer Documentation

## Requirements

This module requires the following module:

- [Vertex AI Search](https://www.drupal.org/project/vertex_ai_search)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration and Usage

Please refer to the documentation for [Creating a Promoted Search Result](https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/vertex-ai-search-promoted-results/creating-a-promoted-search-result) for guidance on module configuration and usage. 


## Maintainers

- Ari Hylas - [ari.saves](https://www.drupal.org/u/arisaves)
- Michael Kinnunen - [mkinnune](https://www.drupal.org/u/mkinnune)
- Sam Lerner - [SamLerner](https://www.drupal.org/u/samlerner)
- Timo Zura - [TimoZura](https://www.drupal.org/u/timozura)

## Developer Documentation

If you wish to contribute, the [Developer Documentation](https://www.drupal.org/docs/extending-drupal/contributed-modules/contributed-module-documentation/vertex-ai-search-promoted-results/developer-documentation) on Drupal.org provides information on the following:
 - Setting up a local development environment using DDEV.
