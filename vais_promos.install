<?php

/**
 * @file
 * Vertex AI Search Promos installation file.
 */

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Implements hook_update_N().
 *
 * Adds support for external url promotions.
 */
function vais_promos_update_100101(&$sandbox) {

  $entityType = 'vais_promo';

  $definitionManager = \Drupal::service('entity.definition_update_manager');

  // Create promoted result external link field.
  $newField = BaseFieldDefinition::create('string')
    ->setLabel(t('External Link'))
    ->setDescription(t('Provide the promoted result external link.'))
    ->setRequired(FALSE)
    ->setSettings([
      'default_value' => '',
      'max_length' => 255,
      'text_processing' => 0,
    ])
    ->setDisplayOptions('view', [
      'label' => 'hidden',
      'type' => 'string',
      'weight' => -4,
    ])
    ->setDisplayOptions('form', [
      'type' => 'string_textfield',
      'weight' => -4,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);

  // Install the new definition.
  $definitionManager->installFieldStorageDefinition('promo_link', $entityType, $entityType, $newField);

  // Create Promoted Result Type Field.
  $newField = BaseFieldDefinition::create('list_string')
    ->setLabel(t('Promoted Result Type'))
    ->setDescription(t('The type of Promoted Result.'))
    ->setRequired(TRUE)
    ->setSettings([
      'allowed_values' => [
        'internal' => t('Internal'),
        'external' => t('External'),
      ],
    ])
    ->setDisplayOptions('view', [
      'label' => 'visible',
      'type' => 'list_default',
      'weight' => -5,
    ])
    ->setDisplayOptions('form', [
      'type' => 'options_select',
      'weight' => -5,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayConfigurable('form', TRUE);

  // Install the new definition.
  $definitionManager->installFieldStorageDefinition('promo_type', $entityType, $entityType, $newField);

  // Update existing 'promo_content' field settings.
  $fieldDefinition = $definitionManager->getFieldStorageDefinition('promo_content', $entityType);
  $fieldDefinition->setLabel(t('Content Reference'));
  $fieldDefinition->setDescription(t('Reference the promoted content.'));
  $fieldDefinition->setRequired(FALSE);
  $definitionManager->updateFieldStorageDefinition($fieldDefinition);

  // Update existing 'promo_title_override' field settings.
  $fieldDefinition = $definitionManager->getFieldStorageDefinition('promo_title_override', $entityType);
  $fieldDefinition->setLabel(t('Promotion Title'));
  $fieldDefinition->setDescription(t('The displayed title of the promoted result.'));
  $fieldDefinition->setRequired(TRUE);
  $definitionManager->updateFieldStorageDefinition($fieldDefinition);

}
